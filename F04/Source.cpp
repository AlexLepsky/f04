#include <iostream>
#include <fstream>
#include <string>

using namespace std;

const int arabskie[] = { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
const std::string rimskiye[] = { "I", "IV", "V", "IX", "X", "XL", "L",
"XC", "C", "CD", "D", "CM", "M" };

bool isNum(const string& str)
{
	for (int i = 0; i < str.size(); ++i)
		if (!isdigit(str[i]))
			return false;
	return true;
}


int slovo(string& str, string& copy)
{
	int a = -1, b = -1;
	for (int i = 0; i < str.size(); ++i)
		if (str[i] == '.') {
			a = i;
			for (int j = str.size() - 1; j > a; --j)
				if (str[j] == '.') {
					i = str.size();
					b = j;
					break;
				}
		}

	if (a != -1 && b != -1 && isNum(str.substr(0, a - 1)) && isNum(str.substr(a + 1, b - a - 1))
		&& isNum(str.substr(b + 1))) {

		copy = str.substr(0, b + 1);
		return stoi(str.substr(b + 1));
	}
	return 0;
}

int main()
{
	ifstream fs("x.txt");
	if (!fs.is_open())
	{
		cout << "error";
		return 1;
	}

	int chislo;
	string otvet = "", copy;

	for (string rim = ""; fs >> rim;) {

		if (chislo = slovo(rim, copy)) {
			int i = 12;
			string numm = "";
			while (chislo > 0 && i) {
				if (chislo >= arabskie[i]) {
					numm += rimskiye[i];
					chislo -= arabskie[i];
				}
				else i--;
			}
			while (chislo > 0) {
				numm += rimskiye[0];
				chislo--;
			}
			rim = copy + numm;
		}
		otvet += rim + char(fs.peek());
	}

	fs.close();

	ofstream ofs("x.txt", ios::out | ios::trunc);
	otvet.pop_back();

	ofs << otvet;

	ofs.close();
	cout << "gotovo!!!!!";
	system("pause");
	return 0;
}